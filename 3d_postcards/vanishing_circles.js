window.onload = function () {
    var canvas = document.getElementById("canvas"),
        context = canvas.getContext("2d"),
        width = canvas.width = window.innerWidth,
        height = canvas.height = window.innerHeight,
        fl = 300,
        circles = [],
        numCircles = 200,
        centerZ = 1000,
        baseAngle = 0,
        rotationSpeed = 0.01;


    for (var i = 0; i < numCircles; i++) {
        var circle = {
            angle: utils.randomRange(0, Math.PI * 2),
            radius: utils.randomRange(100, 1100),
            y: utils.randomRange(2000, -2000),
        };

        circle.x = Math.cos(circle.angle + baseAngle) * circle.radius;
        circle.z = centerZ + Math.sin(circle.angle + baseAngle) * circle.radius;
        circles.push(circle);
    }

    context.translate(width / 2, height / 2);
    context.fillStyle = "black";

    document.body.addEventListener("mousemove", function (event) {
        rotationSpeed = (event.clientX - width / 2) * 0.00005;
    });

    update();

    function update() {
        baseAngle += rotationSpeed;

        circles.sort(zsort);

        context.clearRect(-width / 2, -height / 2, width, height);

        for (var i = 0; i < numCircles; i++) {
            var circle = circles[i],
                perspective = fl / (fl + circle.z);

            context.save();
            context.scale(perspective, perspective);
            context.translate(circle.x, circle.y);
            context.globalAlpha = utils.map(circle.y, 2000, -2000, 1, 0);
            
            context.beginPath();
            context.arc(0, 0, 40, 0, Math.PI * 2, false);
            context.fill();

            context.restore();

            circle.x = Math.cos(circle.angle + baseAngle) * circle.radius;
            circle.z = centerZ + Math.sin(circle.angle + baseAngle) * circle.radius;

            circle.y -= 10;
            if (circle.y < -2000) {
                circle.y = 2000;
            }
        }

        requestAnimationFrame(update);
    }

    function zsort(cardA, cardB) {
        return cardB.z - cardA.z;
    }

};