window.onload = function () {
    var canvas = document.getElementById("canvas"),
        context = canvas.getContext("2d"),
        width = canvas.width = window.innerWidth,
        height = canvas.height = window.innerHeight;

    var centerX = width * .5,
        centerY = height * 0.5,
        xRadius = 200,
        yRadius = 100,
        xSpeed = 0.1,
        xAngle = 0,
        ySpeed = 0.131,
        yAngle = 0;

    render();

    function render() {
        var x = centerX + Math.cos(xAngle) * xRadius;
        var y = centerY + Math.sin(yAngle) * yRadius;

        context.clearRect(0, 0, width, height);
        context.beginPath();
        context.arc(x, y, 10, 0, Math.PI * 2, false);
        context.fill();

        xAngle += xSpeed;
        yAngle += ySpeed;

        requestAnimationFrame(render);
    }
};