window.onload = function () {
    var canvas = document.getElementById("canvas"),
        context = canvas.getContext("2d"),
        width = canvas.width = window.innerWidth,
        height = canvas.height = window.innerHeight;

    context.translate(width/2, height / 2);
    context.scale(1, -1);

    for (var angle = 0; angle < Math.PI * 2; angle += 0.0001) {

        var x = Math.cos(angle) * 200,
            y = Math.sin(angle) * 200;

        context.fillRect(x, y, 5, 5);
    }
};