window.onload = function () {
    var canvas = document.getElementById("canvas"),
        context = canvas.getContext("2d"),
        width = canvas.width = window.innerWidth,
        height = canvas.height = window.innerHeight;

    var centerX = width * .5,
        centerY = height * 0.5,
        baseRadius = 100,
        baseAlpha = 0.5,
        offset = height * .4,
        arcOffset = 50,
        alphaOffset = 0.5,
        speed = 0.1,
        angle = 0;

    render();

    function render() {
        var y = centerY + Math.sin(angle) * offset;
        var arcSize = baseRadius + Math.sin(angle) * arcOffset;
        var alpha = baseAlpha + Math.sin(angle) * alphaOffset;

        context.fillStyle = "rgba(0, 0, 0, " + alpha + ")";
        context.clearRect(0, 0, width, height);
        context.beginPath();
        context.arc(centerX, y, arcSize, 0, Math.PI * 2, false);
        context.fill();

        angle += speed;

        requestAnimationFrame(render);
    }
};