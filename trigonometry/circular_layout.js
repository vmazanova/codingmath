window.onload = function () {
    var canvas = document.getElementById("canvas"),
        context = canvas.getContext("2d"),
        width = canvas.width = window.innerWidth,
        height = canvas.height = window.innerHeight;

    var centerX = width * .5,
        centerY = height * 0.5,
        radius = 200,
        angle = 0,
        numOfObjects = 20,
        slice = Math.PI * 2 / numOfObjects;

    for (var i = 0; i < numOfObjects; i++) {
        angle = i * slice;

        var x = centerX + Math.cos(angle) * radius;
        var y = centerY + Math.sin(angle) * radius;

        context.beginPath();
        context.arc(x, y, 10, 0, Math.PI * 2, false);
        context.fill();
    }
};