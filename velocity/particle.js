var particle = {
    _position: null,
    _velocity: null,

    create: function (x, y, speed, direction) {
        var obj = Object.create(this);

        obj._position = vector.create(x, y);
        obj._velocity = vector.create(0, 0);
        obj._velocity.setLength(speed);
        obj._velocity.setAngle(direction);

        return obj;
    },

    update: function () {
        this._position.addTo(this._velocity);
    }
};