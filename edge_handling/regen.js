// object pooling
window.onload = function () {
    var canvas = document.getElementById("canvas"),
        context = canvas.getContext("2d"),
        width = canvas.width = window.innerWidth,
        height = canvas.height = window.innerHeight,
        particles = [],
        numParticles = 100;

    createParticle();

    update();

    function update() {
        context.clearRect(0, 0, width, height);

        for (var i = 0; i < particles.length; i++) {
            var p = particles[i];
            p.update();

            context.beginPath();
            context.arc(p._position.getX(), p._position.getY(), p._radius, 0, Math.PI * 2, false);
            context.fill();

            if (p._position.getY() - p._radius > height) {
                p._position.setX(width / 2);
                p._position.setY(height);
                p._velocity.setLength(Math.random() * 8 + 5);
                p._velocity.setAngle(-Math.PI / 2 + (Math.random() * .2 - .1));
            }
        }

        requestAnimationFrame(update);
    }

    function createParticle() {
        var speed = Math.random() * 8 + 5,
            direction = -Math.PI / 2 + (Math.random() * .2 - .1);

        var p = particle.create(width / 2, height, speed, direction, 0.1);
        p._radius = Math.random() * 10 + 2;
        particles.push(p);

        if (particles.length < numParticles) {
            requestAnimationFrame(createParticle);
        }
    }
};