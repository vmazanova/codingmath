window.onload = function () {
    var canvas = document.getElementById("canvas"),
        context = canvas.getContext("2d"),
        width = canvas.width = window.innerWidth,
        height = canvas.height = window.innerHeight,
        p = particle.create(width / 2, height / 2, 3, Math.random() * Math.PI * 2);

    p._radius = 50;

    update();

    function update() {
        context.clearRect(0, 0, width, height);

        p.update();

        context.beginPath();
        context.arc(p._position.getX(), p._position.getY(), p._radius, 0, Math.PI * 2, false);
        context.fill();

        if (p._position.getX() - p._radius > width) {
            p._position.setX(-p._radius);
        }
        if (p._position.getX() + p._radius < 0) {
            p._position.setX(width + p._radius);
        }
        if (p._position.getY() - p._radius > height) {
            p._position.setY(-p._radius);
        }
        if (p._position.getY() + p._radius < 0) {
            p._position.setY(height + p._radius);
        }

        requestAnimationFrame(update);
    }
};