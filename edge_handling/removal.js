window.onload = function () {
    var canvas = document.getElementById("canvas"),
        context = canvas.getContext("2d"),
        width = canvas.width = window.innerWidth,
        height = canvas.height = window.innerHeight,
        particles = [];

    for (var i = 0; i < 100; i++) {
        var speed = Math.random() * 5 + 2,
            direction = Math.random() * Math.PI * 2;

        var p = particle.create(width / 2, height / 3, speed, direction);
        p._radius = 10;
        particles.push(p);
    }

    update();

    function update() {
        context.clearRect(0, 0, width, height);

        console.log(particles.length);

        for (var i = 0; i < particles.length; i++) {
            var p = particles[i];
            p.update();

            context.beginPath();
            context.arc(p._position.getX(), p._position.getY(), p._radius, 0, Math.PI * 2, false);
            context.fill();
        }

        removeDeadParticles();

        requestAnimationFrame(update);
    }

    function removeDeadParticles() {
        for (var i = particles.length - 1; i >= 0; i--) {
            var p = particles[i];

            if (p._position.getX() - p._radius > width ||
                p._position.getX() + p._radius < 0 ||
                p._position.getY() - p._radius > height ||
                p._position.getY() + p._radius < 0) {
                particles.splice(i, 1);
            }
        }
    }
};