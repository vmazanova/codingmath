var particle = {
    _position: null,
    _velocity: null,
    _gravity: null,

    create: function (x, y, speed, direction, grav) {
        var obj = Object.create(this);

        obj._position = vector.create(x, y);
        obj._velocity = vector.create(0, 0);
        obj._velocity.setLength(speed);
        obj._velocity.setAngle(direction);
        obj._gravity = vector.create(0, grav || 0);

        return obj;
    },

    update: function () {
        this._velocity.addTo(this._gravity);
        this._position.addTo(this._velocity);
    },

    accelerate: function (accel) {
        this._velocity.addTo(accel);
    }
};