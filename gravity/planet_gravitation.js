window.onload = function () {
    var canvas = document.getElementById("canvas"),
        context = canvas.getContext("2d"),
        width = canvas.width = window.innerWidth,
        height = canvas.height = window.innerHeight,
        sun = particle.create(width / 2, height / 2, 0, 0),
        planet = particle.create(width / 2 + 200, height / 2, 10, -Math.PI / 2);

    sun._mass = 20000;

    update();

    function update() {
        context.clearRect(0, 0, width, height);

        planet.gravitateTo(sun);
        planet.update();

        context.beginPath();
        context.fillStyle = "#ffff00";
        context.arc(sun._position.getX(), sun._position.getY(), 50, 0, Math.PI * 2, false);
        context.fill();

        context.beginPath();
        context.fillStyle = "#0000ff";
        context.arc(planet._position.getX(), planet._position.getY(), 10, 0, Math.PI * 2, false);
        context.fill();


        requestAnimationFrame(update);
    }
};