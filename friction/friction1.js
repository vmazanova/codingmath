window.onload = function () {
    var canvas = document.getElementById("canvas"),
        context = canvas.getContext("2d"),
        width = canvas.width = window.innerWidth,
        height = canvas.height = window.innerHeight,
        p = particle.create(width / 2, height / 2, 10, Math.random() * Math.PI * 2),
        friction = vector.create(0.15, 0);

    p._radius = 10;

    update();

    function update() {
        context.clearRect(0, 0, width, height);

        friction.setAngle(p._velocity.getAngle());

        if (p._velocity.getLength() <= friction.getLength()) {
            p._velocity.setLength(0);
        } else {
            p._velocity.subtractFrom(friction);
        }

        p.update();

        context.beginPath();
        context.arc(p._position.getX(), p._position.getY(), p._radius, 0, Math.PI * 2, false);
        context.fill();


        requestAnimationFrame(update);
    }
};