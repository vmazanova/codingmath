var particle = {
    _position: null,
    _velocity: null,
    _gravity: null,
    _mass: 1,
    _radius: 0,
    _bounce: -1,
    _friction: 1,

    create: function (x, y, speed, direction, grav) {
        var obj = Object.create(this);

        obj._position = vector.create(x, y);
        obj._velocity = vector.create(0, 0);
        obj._velocity.setLength(speed);
        obj._velocity.setAngle(direction);
        obj._gravity = vector.create(0, grav || 0);

        return obj;
    },

    update: function () {
        this._velocity.multiplyBy(this._friction);
        this._velocity.addTo(this._gravity);
        this._position.addTo(this._velocity);
    },

    accelerate: function (accel) {
        this._velocity.addTo(accel);
    },

    angleTo: function (p2) {
        return Math.atan2(p2._position.getY() - this._position.getY(), p2._position.getX() - this._position.getX());
    },

    distanceTo: function (p2) {
        var dx = p2._position.getX() - this._position.getX(),
            dy = p2._position.getY() - this._position.getY();

        return Math.sqrt(dx * dx + dy * dy);
    },

    gravitateTo: function (p2) {
        var grav = vector.create(0, 0),
            dist = this.distanceTo(p2);

        grav.setLength(p2._mass / (dist * dist));
        grav.setAngle(this.angleTo(p2));

        this._velocity.addTo(grav);
    }
};