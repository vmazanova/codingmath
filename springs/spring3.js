window.onload = function () {
    var canvas = document.getElementById("canvas"),
        context = canvas.getContext("2d"),
        width = canvas.width = window.innerWidth,
        height = canvas.height = window.innerHeight,
        particleA = particle.create(utils.randomRange(0, width), utils.randomRange(0, height), utils.randomRange(0, 50), utils.randomRange(0, Math.PI * 2), 0.5),
        particleB = particle.create(utils.randomRange(0, width), utils.randomRange(0, height), utils.randomRange(0, 50), utils.randomRange(0, Math.PI * 2), 0.5),
        particleC = particle.create(utils.randomRange(0, width), utils.randomRange(0, height), utils.randomRange(0, 50), utils.randomRange(0, Math.PI * 2), 0.5),
        k = 0.01,
        separation = 200;

    particleA._radius = 20;
    particleA._friction = 0.9;

    particleB._radius = 20;
    particleB._friction = 0.9;

    particleC._radius = 20;
    particleC._friction = 0.9;

    update();

    function update() {
        context.clearRect(0, 0, width, height);

        spring(particleA, particleB, separation);
        spring(particleB, particleC, separation);
        spring(particleC, particleA, separation);

        particleA.update();
        particleB.update();
        particleC.update();

        handleEdge(particleA);
        handleEdge(particleB);
        handleEdge(particleC);

        context.beginPath();
        context.arc(particleA._position.getX(), particleA._position.getY(), particleA._radius, 0, Math.PI * 2, false);
        context.fill();

        context.beginPath();
        context.arc(particleB._position.getX(), particleB._position.getY(), particleB._radius, 0, Math.PI * 2, false);
        context.fill();

        context.beginPath();
        context.arc(particleC._position.getX(), particleC._position.getY(), particleC._radius, 0, Math.PI * 2, false);
        context.fill();

        context.beginPath();
        context.moveTo(particleA._position.getX(), particleA._position.getY());
        context.lineTo(particleB._position.getX(), particleB._position.getY());
        context.lineTo(particleC._position.getX(), particleC._position.getY());
        context.lineTo(particleA._position.getX(), particleA._position.getY());
        context.stroke();


        requestAnimationFrame(update);
    }

    function spring(p0, p1, separation) {
        var distance = p0._position.subtract(p1._position);
        distance.setLength(distance.getLength() - separation);

        var springForce = distance.multiply(k);
        p1._velocity.addTo(springForce);
        p0._velocity.subtractFrom(springForce);
    }

    function handleEdge(p) {
        if (p._position.getY() + p._radius > height) {
            p._position.setY(height - p._radius);
            p._velocity.setY(p._velocity.getY() * -0.95);
        }
    }
};