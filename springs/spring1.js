window.onload = function () {
    var canvas = document.getElementById("canvas"),
        context = canvas.getContext("2d"),
        width = canvas.width = window.innerWidth,
        height = canvas.height = window.innerHeight,
        springPoint = vector.create(width / 2, height / 2),
        weight = particle.create(Math.random() * width, Math.random() * height, 50, Math.random() * Math.PI * 2),
        k = 0.5;

    weight._radius = 20;
    weight._friction = 0.9;

    document.body.addEventListener("mousemove", function (event) {
        springPoint.setX(event.clientX);
        springPoint.setY(event.clientY);
    });

    update();

    function update() {
        context.clearRect(0, 0, width, height);

        var distance = springPoint.subtract(weight._position),
            springForce = distance.multiply(k);

        weight._velocity.addTo(springForce);

        weight.update();

        context.beginPath();
        context.arc(weight._position.getX(), weight._position.getY(), weight._radius, 0, Math.PI * 2, false);
        context.fill();

        context.beginPath();
        context.arc(springPoint.getX(), springPoint.getY(), 5, 0, Math.PI * 2, false);
        context.fill();

        context.beginPath();
        context.moveTo(springPoint.getX(), springPoint.getY());
        context.lineTo(weight._position.getX(), weight._position.getY());
        context.stroke();


        requestAnimationFrame(update);
    }
};