window.onload = function () {
    var canvas = document.getElementById("canvas"),
        context = canvas.getContext("2d"),
        width = canvas.width = window.innerWidth,
        height = canvas.height = window.innerHeight,
        points = [],
        numPoints = 100;

    for (var i = 0; i < numPoints; i++) {
        var p = {
            x: utils.randomRange(0, width),
            y: utils.randomRange(0, height)
        };

        context.beginPath();
        context.arc(p.x, p.y, 3, 0, Math.PI * 2, false);
        context.fill();

        points.push(p);
    }

    context.strokeStyle = "lightgray";
    context.beginPath();
    context.moveTo(points[0].x, points[0].y);
    for (var i = 0; i < numPoints; i++) {
        context.lineTo(points[i].x, points[i].y);
    }
    context.stroke();

    context.strokeStyle = "black";
    context.beginPath();
    utils.multicurve(points, context);
    context.stroke();

};